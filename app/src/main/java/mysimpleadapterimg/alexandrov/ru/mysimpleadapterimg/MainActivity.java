package mysimpleadapterimg.alexandrov.ru.mysimpleadapterimg;
//
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    int [] myData = {1,3,-4,6,-6,-3,2};
    ListView lvSimple;

    //имена атрибутов для Мар
    final String MY_TEXT = "text";
    final String MY_VALUE = "value";
    final String MY_IMG = "img" ;
    //
    // картинки для отображения динамики
    final int positive = android.R.drawable.arrow_up_float;
    final int negative = android.R.drawable.arrow_down_float;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Map<String,Object>> data = new ArrayList<Map<String, Object>>(myData.length);

        Map<String, Object> map;
        int img =0;
        for (int i = 0 ;i < myData.length; i++){
             map = new HashMap<String,Object>();
             map.put(MY_TEXT,"День номенр  " + i);
             map.put(MY_VALUE,myData[i]);
             if (myData[i]==0){
                 img = 0;
             } else{
                if (myData[i]>0){
                    img = positive;
                }else{
                    img = negative;
                }
             }
            map.put(MY_IMG, img);
            data.add(map);

         }



        //создаем массив атрибутов откуда будут читаться даные
        String [] from = {MY_TEXT,MY_VALUE,MY_IMG};

        //создаем массив в который будут писаться данные
        int [] to = {R.id.tvText, R.id.tvValue, R.id.ivImg};

        //Создаем  адаптер
        MySimpleAdapter sAdapter = new MySimpleAdapter(this,data, R.layout.item, from, to);


        // определяем список и присваеваем ему адаптер

        lvSimple = (ListView)findViewById(R.id.lvSimple);
        lvSimple.setAdapter(sAdapter);



    }


    class MySimpleAdapter extends SimpleAdapter{
            public MySimpleAdapter(Context context, List<? extends Map<String, ?>> data,
                               int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
        }

        @Override
        public void setViewText(TextView v, String text) {
            super.setViewText(v, text);
            // если нужный нам TextView, то разрисовываем
            if (v.getId() == R.id.tvValue) {
                int i = Integer.parseInt(text);
                if (i < 0) v.setTextColor(Color.RED); else
                if (i > 0) v.setTextColor(Color.GREEN);
            }

        }

        @Override
        public void setViewImage(ImageView v, int value) {
            super.setViewImage(v, value);
            if (value == negative) v.setBackgroundColor(Color.RED); else
            if (value == positive) v.setBackgroundColor(Color.GREEN);
        }
    }
}


























